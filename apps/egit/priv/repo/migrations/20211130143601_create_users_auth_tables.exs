defmodule EGit.Repo.Migrations.CreateUsersAuthTables do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string, collate: :nocase
      add :domain, :stirng, collate: :nocase
      add :hashed_password, :string
      add :uid, :string, null: false, collate: :nocase
      add :name, :string
      add :confirmed_at, :naive_datetime
      timestamps()
    end

    create unique_index(:users, [:uid])
    create unique_index(:users, [:email])
    create unique_index(:users, [:domain])

    create table(:users_tokens) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :token, :binary, null: false, size: 32
      add :context, :string, null: false
      add :sent_to, :string
      timestamps(updated_at: false)
    end

    create index(:users_tokens, [:user_id])
    create unique_index(:users_tokens, [:context, :token])
  end
end
