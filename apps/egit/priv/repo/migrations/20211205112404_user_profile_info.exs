defmodule EGit.Repo.Migrations.UserProfileInfo do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :pronouns, :string
      add :status, :string
      add :job, :string
      add :location, :string
      add :bio, :string
    end
  end
end
