defmodule EGit.Repo.Migrations.Repositiories do
  use Ecto.Migration

  def change do
    create table(:projects) do
      add :name, :string, null: false
      add :path, :string
      add :description, :string
      add :creator_id, references(:users, on_delete: :nilify_all)
      add :archived, :boolean, default: false

      timestamps()
    end

    create index(:projects, [:name])
    create unique_index(:projects, [:path])

    create table(:project_features) do
      add :project_id, references(:projects, on_delete: :delete_all), null: false
      add :project_access_level, :integer, default: 0

      timestamps()
    end

    create index(:project_features, [:project_id])

    create table(:project_authorization) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :project_id, references(:projects, on_delete: :delete_all), null: false
      add :access_level, :integer, default: 0

      add :expires, :utc_datetime
      timestamps()
    end

    create index(:project_authorization, [:user_id])
    create index(:project_authorization, [:project_id])
  end
end
