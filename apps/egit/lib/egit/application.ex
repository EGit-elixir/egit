defmodule EGit.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      EGit.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: EGit.PubSub},
      EGit.Git.RPC
      # Start a worker by calling: EGit.Worker.start_link(arg)
      # {EGit.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: EGit.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
