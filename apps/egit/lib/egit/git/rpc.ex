defmodule EGit.Git.RPC do
  use GenServer

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  @impl true
  def init(_opt \\ []) do
    # TODO: config
    {:ok, rpc} = GRPC.Stub.connect("192.168.242.101:8124")
    {:ok, %{repo: rpc}}
  end

  def get_rpc(:repo) do
    GenServer.call(__MODULE__, :repo)
  end

  @impl true
  def handle_call(:repo, _from, state) do
    {:reply, Map.get(state, :repo), state}
  end

  @impl true
  def handle_info({:gun_down, _pid, _part, _reason, _opts}, state) do
    {:noreply, state}
  end

  @impl true
  def handle_info({:gun_up, _pid, _part}, state) do
    {:noreply, state}
  end
end
