defmodule EGit.Git.Project do
  use Ecto.Schema
  import Ecto.Changeset

  schema "projects" do
    field :name, :string
    field :path, :string
    field :description, :string
    belongs_to :creator, EGit.Accounts.User
    field :archived, :boolean

    timestamps()
  end

  def create_changeset(project, attrs) do
    project
    |> cast(attrs, [:name, :path, :description, :archived])
    |> validate_required([:path, :creator_id])
    |> validate_name
    |> validate_path
  end

  def create_changeset_with_path(project, int_attrs, attrs) do
    project
    |> cast(int_attrs, [:name, :path, :creator_id])
    |> cast(attrs, [:description, :archived])
    |> validate_required([:path, :creator_id])
    |> validate_name
    |> validate_path
  end

  defp validate_name(changeset) do
    changeset
    |> validate_required([:name])
    |> validate_length(:name, min: 3, max: 250)
  end

  defp validate_path(changeset) do
    changeset
    |> validate_length(:path, min: 10, max: 250)
    # TODO: can we get rid of this? unsafe is unhappy
    |> unsafe_validate_unique(:path, EGit.Repo)
    |> unique_constraint(:pathℓ)
  end
end
