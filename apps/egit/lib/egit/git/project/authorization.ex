defmodule EGit.Git.Project.Authorization do
  use Ecto.Schema

  schema "project_authorization" do
    belongs_to :user, EGit.Accounts.User
    belongs_to :project, EGit.Git.Project
    field :access_level, :integer
    field :expires, :utc_datetime

    timestamps()
  end
end
