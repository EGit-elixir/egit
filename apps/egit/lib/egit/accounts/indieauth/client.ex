defmodule EGit.Accounts.Indieauth.Client do
  use Tesla
  @moduledoc false

  # TODO: config?
  plug(Tesla.Middleware.Timeout, timeout: 1_000)

  def get_url(url) do
    get(url)
  end

  def post_url(url, body) do
    post(url, body)
  end
end
