defmodule EGit.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    # migration 20211130143601
    field :email, :string
    field :domain, :string
    field :password, :string, virtual: true, redact: true
    field :hashed_password, :string, redact: true
    field :uid, :string
    field :name, :string
    field :confirmed_at, :naive_datetime
    timestamps()

    # migration 20211205112404
    field :pronouns, :string
    field :status, :string
    field :job, :string
    field :location, :string
    field :bio, :string
  end

  @doc """
  A user changeset for registration.

  It is important to validate the length of both email and password.
  Otherwise databases may truncate the email without warnings, which
  could lead to unpredictable or insecure behaviour. Long passwords may
  also be very expensive to hash for certain algorithms.

  ## Options

    * `:hash_password` - Hashes the password so it can be stored securely
      in the database and ensures the password field is cleared to prevent
      leaks in the logs. If password hashing is not needed and clearing the
      password field is not desired (like when using this changeset for
      validations on a LiveView form), this option can be set to `false`.
      Defaults to `true`.
  """
  def registration_changeset(user, attrs, opts \\ []) do
    user
    |> cast(attrs, [:uid, :email, :password])
    |> validate_uid()
    |> validate_email()
    |> validate_password(opts)
  end

  def registration_changeset_domain(user, attrs) do
    user
    |> cast(attrs, [:uid, :domain])
    |> validate_uid()
    |> validate_domain()
  end

  defp maybe_hash_password(changeset, opts) do
    hash_password? = Keyword.get(opts, :hash_password, true)
    password = get_change(changeset, :password)

    if hash_password? && password && changeset.valid? do
      changeset
      # If using Bcrypt, then further validate it is at most 72 bytes long
      |> validate_length(:password, max: 72, count: :bytes)
      |> put_change(:hashed_password, Bcrypt.hash_pwd_salt(password))
      |> delete_change(:password)
    else
      changeset
    end
  end

  @doc """
  A user changeset for changing the email.

  It requires the email to change otherwise an error is added.
  """
  def email_changeset(user, attrs) do
    user
    |> cast(attrs, [:email])
    |> validate_email()
    |> case do
      %{changes: %{email: _}} = changeset -> changeset
      %{} = changeset -> add_error(changeset, :email, "did not change")
    end
  end

  @doc """
  A user changeset for changing the password.

  ## Options

    * `:hash_password` - Hashes the password so it can be stored securely
      in the database and ensures the password field is cleared to prevent
      leaks in the logs. If password hashing is not needed and clearing the
      password field is not desired (like when using this changeset for
      validations on a LiveView form), this option can be set to `false`.
      Defaults to `true`.
  """
  def password_changeset(user, attrs, opts \\ []) do
    user
    |> cast(attrs, [:password])
    |> validate_confirmation(:password, message: "does not match password")
    |> validate_password(opts)
  end

  @doc """
  A user changeset for changing profile informations
  """
  def profile_changeset(user, attrs) do
    user
    |> cast(attrs, [:pronouns, :status, :job, :location, :bio, :name])
    |> validate_name
    # |> validate_pronouns
    |> validate_status
    |> validate_job
    |> validate_location
    |> validate_bio
  end

  @doc """
  Confirms the account by setting `confirmed_at`.
  """
  def confirm_changeset(user) do
    now = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
    change(user, confirmed_at: now)
  end

  @doc """
  Verifies the password.

  If there is no user or the user doesn't have a password, we call
  `Bcrypt.no_user_verify/0` to avoid timing attacks.
  """
  def valid_password?(%EGit.Accounts.User{hashed_password: hashed_password}, password)
      when is_binary(hashed_password) and byte_size(password) > 0 do
    Bcrypt.verify_pass(password, hashed_password)
  end

  def valid_password?(_, _) do
    Bcrypt.no_user_verify()
    false
  end

  defp validate_uid(changeset) do
    changeset
    |> validate_required([:uid])
    |> validate_format(:uid, ~r/^[A-Za-z0-9_]+$/, message: "invalid user name")
    |> validate_length(:uid, min: 3, max: 50)
    |> unsafe_validate_unique(:uid, EGit.Repo)
    |> unique_constraint(:uid)
  end

  defp validate_email(changeset) do
    changeset
    |> validate_format(:email, ~r/^[^\s]+@[^\s]+$/, message: "must have the @ sign and no spaces")
    |> validate_length(:email, max: 160)
    |> unsafe_validate_unique(:email, EGit.Repo)
    |> unique_constraint(:email)
  end

  defp validate_domain(changeset) do
    changeset
    |> validate_required([:domain])
  end

  defp validate_password(changeset, opts) do
    changeset
    # |> validate_required([:password])
    |> validate_length(:password, min: 12, max: 72)
    |> validate_format(:password, ~r/[a-z]/, message: "at least one lower case character")
    |> validate_format(:password, ~r/[A-Z]/, message: "at least one upper case character")
    |> validate_format(:password, ~r/[!?@#$%^&*_0-9]/,
      message: "at least one digit or punctuation character"
    )
    |> maybe_hash_password(opts)
  end

  defp validate_name(changeset) do
    changeset
    |> validate_length(:name, min: 3, max: 72)
  end

  defp validate_status(changeset) do
    changeset
    |> validate_length(:status, min: 3, max: 50)
  end

  defp validate_job(changeset) do
    changeset
    |> validate_length(:job, min: 3, max: 72)
  end

  defp validate_location(changeset) do
    changeset
    |> validate_length(:location, min: 3, max: 72)
  end

  defp validate_bio(changeset) do
    changeset
    |> validate_length(:bio, min: 25, max: 250)
  end

  @doc """
  Validates the current password otherwise adds an error to the changeset.
  """
  def validate_current_password(changeset, password) do
    if valid_password?(changeset.data, password) do
      changeset
    else
      add_error(changeset, :current_password, "is not valid")
    end
  end
end
