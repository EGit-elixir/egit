defmodule EGitWeb.UserSettingsSshController do
  use EGitWeb, :controller
  alias EGit.Accounts
  alias EGitWeb.Router.Helpers, as: Routes

  # FIXME: don't crashe on invalid integer as id

  def index(conn, params) do
    {id, _} =
      Map.get(params, "id", "0")
      |> Integer.parse()

    user =
      conn
      |> Accounts.get_user()

    keys = Accounts.get_ssh_keys(user, id)
    # |> Enum.to_list

    conn
    |> render("index.html", keys: keys, page: id)
  end

  def new(conn, _params) do
    user =
      conn
      |> Accounts.get_user()

    render(conn, "new.html", changeset: Accounts.ssh_key_new_changeset(user))
  end

  def create(conn, %{"key" => key_params}) do
    user =
      conn
      |> Accounts.get_user()

    case Accounts.register_ssh_key(user, key_params) do
      {:ok, key} ->
        conn
        |> put_flash(:info, "Key added successfully " <> key.fingerprint_sha)
        |> redirect(to: Routes.user_settings_ssh_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> render("new.html", changeset: changeset, page: 0)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    IO.inspect(params)

    user =
      conn
      |> Accounts.get_user()

    {id, _} =
      id
      |> Integer.parse()

    key = Accounts.get_ssh_key_id(user, id)

    if key do
      key
      |> EGit.Repo.delete()

      conn
      |> put_flash(:info, "Key deleted #{key.name}")
      |> redirect(to: Routes.user_settings_ssh_path(conn, :index))
    else
      conn
      |> put_flash(:error, "Failed to delete")
      |> redirect(to: Routes.user_settings_ssh_path(conn, :index))
    end
  end

  def edit(conn, %{"id" => id}) do
    {id, _} = Integer.parse(id)

    user =
      conn
      |> Accounts.get_user()

    key = Accounts.get_ssh_key_id(user, id)

    if key do
      changeset = Accounts.ssh_key_changeset(key)

      conn
      |> render("edit.html", key: key, changeset: changeset)
    else
      conn
      |> put_flash(:error, "Key not found")
      |> redirect(to: Routes.user_settings_ssh_path(conn, :index))
    end
  end

  def update(conn, %{"id" => id, "ssh_key" => key_params}) do
    {id, _} = Integer.parse(id)

    user =
      conn
      |> Accounts.get_user()

    key = Accounts.get_ssh_key_id(user, id)

    if key do
      case Accounts.update_ssh_key(key, key_params) do
        {:ok, key} ->
          conn
          |> put_flash(:info, "Key update: " <> key.name)
          |> redirect(to: Routes.user_settings_ssh_path(conn, :index))

        {:error, %Ecto.Changeset{} = changeset} ->
          IO.inspect(changeset)

          conn
          |> render("edit.html", key: key, changeset: changeset)
      end
    else
      conn
      |> put_flash(:error, "Key not found")
      |> redirect(to: Routes.user_settings_ssh_path(conn, :index))
    end
  end
end
