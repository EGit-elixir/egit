defmodule EGitWeb.ProjectNewController do
  use EGitWeb, :controller
  alias EGit.Accounts
  alias EGit.Git.Projects

  def index(conn, params) do
    changeset =
      %EGit.Git.Project{}
      |> Ecto.Changeset.cast(%{}, [])

    conn
    |> render("index.html", changeset: changeset)
  end

  def create(conn, %{"project" => project_params}) do
    IO.inspect(project_params)

    user =
      conn
      |> Accounts.get_user()

    Projects.create_project(user, Map.get(project_params, "name"), project_params)
    |> case do
      {:ok, project} ->
        conn
        |> put_flash(:info, "created #{project.name}")
        |> redirect(to: "/")

      {:error, {:ecto, changeset}} ->
        conn
        |> render("index.html", changeset: changeset)

      {:error, v} ->
        changeset =
          %EGit.Git.Project{}
          |> Ecto.Changeset.cast(%{}, [])

        conn
        |> put_flash(:error, "Failed to create project")
        |> render("index.html", changeset: changeset)
    end
  end
end
