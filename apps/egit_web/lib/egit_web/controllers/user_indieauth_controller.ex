defmodule EGitWeb.UserIndieauthController do
  use EGitWeb, :controller
  alias EGit.Accounts.Indieauth

  alias EGit.Accounts
  alias EGit.Accounts.User
  alias EGitWeb.UserAuth

  def new(conn, _params) do
    render(conn, "new.html", error_message: nil)
  end

  def create(conn, %{"domain" => domain_params}) do
    %{"domain" => domain} = domain_params

    client_id = EGitWeb.Router.Helpers.url(conn)
    redirect_uri = EGitWeb.Router.Helpers.user_indieauth_url(conn, :callback)

    auth_domain =
      Indieauth.get_auth_endpoint(domain, client_id, redirect_uri, %{"scope" => "profile email"})

    case auth_domain do
      {:ok, auth_domain} ->
        redirect(conn, external: auth_domain)

      {:error, err} ->
        render(conn, "new.html", error_message: print_error(err))
    end
  end

  # FIXME: is this secure? - it is not. find a way to encode the domain which can not be altered
  # TODO: update to LiveView, so username is checked real time if free
  # TODO: append `@` automaticly
  def update(conn, %{"user" => user_params}) do
    case Accounts.register_user_domain(user_params) do
      {:ok, user} ->
        # TODO: mail?
        conn
        |> put_flash(:info, "User created successfully")
        |> UserAuth.log_in_user(user)

      {:error, %Ecto.Changeset{} = changeset} ->
        IO.inspect(changeset)

        render(conn, "register.html", changeset: changeset, domain: Map.get(user_params, "domain"))
    end
  end

  def callback(conn, params) do
    %{"code" => code, "me" => me} = params

    client_id = EGitWeb.Router.Helpers.url(conn)
    redirect_uri = EGitWeb.Router.Helpers.user_indieauth_url(conn, :callback)
    me = Indieauth.verify(code, me, client_id, redirect_uri)

    case me do
      {:ok, domain} -> success_login(conn, domain)
      {:error, err} -> render_error(conn, err)
    end
  end

  # callback succeded, trying to log user in
  def success_login(conn, domain) when is_binary(domain) do
    Accounts.get_user_by_domain(domain)
    |> success_login(conn, domain)
  end

  def success_login(user, conn, domain) when is_nil(user) do
    changeset = Accounts.change_user_registration_indieauth(%User{})
    render(conn, "register.html", changeset: changeset, domain: domain)
  end

  def success_login(user, conn, _domain) do
    conn
    |> put_flash(:info, "User logged in successfully.")
    |> UserAuth.log_in_user(user)
  end

  # error styling functions
  defp render_error(conn, err) do
    render(conn, "new.html", error_message: print_error(err))
  end

  defp print_error(:no_authorization_endpoint) do
    "No authorization endpoint could be found"
  end

  defp print_error({:no_scheme}) do
    "Invalid url scheme"
  end

  defp print_error(:econnrefused) do
    "Could not connect to server"
  end

  defp print_error(err) when is_binary(err) do
    err
  end

  defp print_error(err) when is_atom(err) do
    Atom.to_string(err)
  end
end
