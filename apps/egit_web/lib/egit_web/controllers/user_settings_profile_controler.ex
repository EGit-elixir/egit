defmodule EGitWeb.UserSettingsProfileController do
  use EGitWeb, :controller
  alias EGit.Accounts

  def index(conn, _params) do
    user =
      conn
      |> Accounts.get_user()

    changeset = Accounts.change_user_profile(user)

    conn
    |> render("index.html", user: user, changeset: changeset)
  end

  def update(conn, %{"user" => user_params} = _params) do
    user =
      conn
      |> Accounts.get_user()

    Accounts.change_user_profile(user, user_params)
    |> EGit.Repo.update()
    |> case do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "Saved succesfully")
        |> redirect(to: Routes.user_settings_profile_path(conn, :index))

      {:error, changeset} ->
        # FIXME: remember input on invalid changeset
        conn
        |> render("index.html", user: user, changeset: changeset)
    end
  end
end
