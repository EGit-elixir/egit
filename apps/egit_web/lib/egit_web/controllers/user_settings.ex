defmodule EGitWeb.UserSettings do
  import Plug.Conn
  # import Phoenix.Controller

  # alias EGitWeb.Router.Helpers, as: Routes

  def init(default), do: default

  def current_settings_page(conn, _opts) do
    page =
      Phoenix.Router.route_info(
        conn.private[:phoenix_router],
        conn.method,
        conn.request_path,
        conn.host
      ).plug

    conn
    |> assign(:settings_page, page)
  end
end
