defmodule Gitaly.OperationMsg.Operation do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :UNKNOWN | :MUTATOR | :ACCESSOR

  field(:UNKNOWN, 0)
  field(:MUTATOR, 1)
  field(:ACCESSOR, 2)
end

defmodule Gitaly.OperationMsg.Scope do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :REPOSITORY | :STORAGE

  field(:REPOSITORY, 0)
  field(:STORAGE, 2)
end

defmodule Gitaly.OperationMsg do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          op: Gitaly.OperationMsg.Operation.t(),
          scope_level: Gitaly.OperationMsg.Scope.t()
        }

  defstruct op: :UNKNOWN,
            scope_level: :REPOSITORY

  field(:op, 1, type: Gitaly.OperationMsg.Operation, enum: true)
  field(:scope_level, 2, type: Gitaly.OperationMsg.Scope, json_name: "scopeLevel", enum: true)
end

defmodule Gitaly.PbExtension do
  @moduledoc false
  use Protobuf, syntax: :proto3

  extend(Google.Protobuf.ServiceOptions, :intercepted, 82302, optional: true, type: :bool)

  extend(Google.Protobuf.MethodOptions, :op_type, 82303,
    optional: true,
    type: Gitaly.OperationMsg,
    json_name: "opType"
  )

  extend(Google.Protobuf.MethodOptions, :intercepted_method, 82304,
    optional: true,
    type: :bool,
    json_name: "interceptedMethod"
  )

  extend(Google.Protobuf.FieldOptions, :storage, 91233, optional: true, type: :bool)

  extend(Google.Protobuf.FieldOptions, :repository, 91234, optional: true, type: :bool)

  extend(Google.Protobuf.FieldOptions, :target_repository, 91235,
    optional: true,
    type: :bool,
    json_name: "targetRepository"
  )

  extend(Google.Protobuf.FieldOptions, :additional_repository, 91236,
    optional: true,
    type: :bool,
    json_name: "additionalRepository"
  )
end
