defmodule Gitaly.VoteTransactionResponse.TransactionState do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :COMMIT | :ABORT | :STOP

  field(:COMMIT, 0)
  field(:ABORT, 1)
  field(:STOP, 2)
end

defmodule Gitaly.VoteTransactionRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          transaction_id: non_neg_integer,
          node: String.t(),
          reference_updates_hash: binary
        }

  defstruct repository: nil,
            transaction_id: 0,
            node: "",
            reference_updates_hash: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:transaction_id, 2, type: :uint64, json_name: "transactionId")
  field(:node, 3, type: :string)
  field(:reference_updates_hash, 4, type: :bytes, json_name: "referenceUpdatesHash")
end

defmodule Gitaly.VoteTransactionResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          state: Gitaly.VoteTransactionResponse.TransactionState.t()
        }

  defstruct state: :COMMIT

  field(:state, 1, type: Gitaly.VoteTransactionResponse.TransactionState, enum: true)
end

defmodule Gitaly.StopTransactionRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          transaction_id: non_neg_integer
        }

  defstruct repository: nil,
            transaction_id: 0

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:transaction_id, 2, type: :uint64, json_name: "transactionId")
end

defmodule Gitaly.StopTransactionResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.RefTransaction.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.RefTransaction"

  rpc(:VoteTransaction, Gitaly.VoteTransactionRequest, Gitaly.VoteTransactionResponse)

  rpc(:StopTransaction, Gitaly.StopTransactionRequest, Gitaly.StopTransactionResponse)
end

defmodule Gitaly.RefTransaction.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.RefTransaction.Service
end
