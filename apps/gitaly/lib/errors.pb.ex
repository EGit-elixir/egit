defmodule Gitaly.AccessCheckError do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          error_message: String.t(),
          protocol: String.t(),
          user_id: String.t(),
          changes: binary
        }

  defstruct error_message: "",
            protocol: "",
            user_id: "",
            changes: ""

  field(:error_message, 1, type: :string, json_name: "errorMessage")
  field(:protocol, 2, type: :string)
  field(:user_id, 3, type: :string, json_name: "userId")
  field(:changes, 4, type: :bytes)
end
