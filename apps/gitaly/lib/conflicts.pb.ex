defmodule Gitaly.ListConflictFilesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          our_commit_oid: String.t(),
          their_commit_oid: String.t(),
          allow_tree_conflicts: boolean
        }

  defstruct repository: nil,
            our_commit_oid: "",
            their_commit_oid: "",
            allow_tree_conflicts: false

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:our_commit_oid, 2, type: :string, json_name: "ourCommitOid")
  field(:their_commit_oid, 3, type: :string, json_name: "theirCommitOid")
  field(:allow_tree_conflicts, 4, type: :bool, json_name: "allowTreeConflicts")
end

defmodule Gitaly.ConflictFileHeader do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commit_oid: String.t(),
          their_path: binary,
          our_path: binary,
          our_mode: integer,
          ancestor_path: binary
        }

  defstruct commit_oid: "",
            their_path: "",
            our_path: "",
            our_mode: 0,
            ancestor_path: ""

  field(:commit_oid, 2, type: :string, json_name: "commitOid")
  field(:their_path, 3, type: :bytes, json_name: "theirPath")
  field(:our_path, 4, type: :bytes, json_name: "ourPath")
  field(:our_mode, 5, type: :int32, json_name: "ourMode")
  field(:ancestor_path, 6, type: :bytes, json_name: "ancestorPath")
end

defmodule Gitaly.ConflictFile do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          conflict_file_payload:
            {:header, Gitaly.ConflictFileHeader.t() | nil} | {:content, binary}
        }

  defstruct conflict_file_payload: nil

  oneof(:conflict_file_payload, 0)

  field(:header, 1, type: Gitaly.ConflictFileHeader, oneof: 0)
  field(:content, 2, type: :bytes, oneof: 0)
end

defmodule Gitaly.ListConflictFilesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          files: [Gitaly.ConflictFile.t()]
        }

  defstruct files: []

  field(:files, 1, repeated: true, type: Gitaly.ConflictFile)
end

defmodule Gitaly.ResolveConflictsRequestHeader do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          our_commit_oid: String.t(),
          target_repository: Gitaly.Repository.t() | nil,
          their_commit_oid: String.t(),
          source_branch: binary,
          target_branch: binary,
          commit_message: binary,
          user: Gitaly.User.t() | nil,
          timestamp: Google.Protobuf.Timestamp.t() | nil
        }

  defstruct repository: nil,
            our_commit_oid: "",
            target_repository: nil,
            their_commit_oid: "",
            source_branch: "",
            target_branch: "",
            commit_message: "",
            user: nil,
            timestamp: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:our_commit_oid, 2, type: :string, json_name: "ourCommitOid")
  field(:target_repository, 3, type: Gitaly.Repository, json_name: "targetRepository")
  field(:their_commit_oid, 4, type: :string, json_name: "theirCommitOid")
  field(:source_branch, 5, type: :bytes, json_name: "sourceBranch")
  field(:target_branch, 6, type: :bytes, json_name: "targetBranch")
  field(:commit_message, 7, type: :bytes, json_name: "commitMessage")
  field(:user, 8, type: Gitaly.User)
  field(:timestamp, 9, type: Google.Protobuf.Timestamp)
end

defmodule Gitaly.ResolveConflictsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          resolve_conflicts_request_payload:
            {:header, Gitaly.ResolveConflictsRequestHeader.t() | nil} | {:files_json, binary}
        }

  defstruct resolve_conflicts_request_payload: nil

  oneof(:resolve_conflicts_request_payload, 0)

  field(:header, 1, type: Gitaly.ResolveConflictsRequestHeader, oneof: 0)
  field(:files_json, 2, type: :bytes, json_name: "filesJson", oneof: 0)
end

defmodule Gitaly.ResolveConflictsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          resolution_error: String.t()
        }

  defstruct resolution_error: ""

  field(:resolution_error, 1, type: :string, json_name: "resolutionError")
end

defmodule Gitaly.ConflictsService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.ConflictsService"

  rpc(
    :ListConflictFiles,
    Gitaly.ListConflictFilesRequest,
    stream(Gitaly.ListConflictFilesResponse)
  )

  rpc(:ResolveConflicts, stream(Gitaly.ResolveConflictsRequest), Gitaly.ResolveConflictsResponse)
end

defmodule Gitaly.ConflictsService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.ConflictsService.Service
end
