defmodule Gitaly.WriteCommitGraphRequest.SplitStrategy do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :SizeMultiple

  field(:SizeMultiple, 0)
end

defmodule Gitaly.GetArchiveRequest.Format do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :ZIP | :TAR | :TAR_GZ | :TAR_BZ2

  field(:ZIP, 0)
  field(:TAR, 1)
  field(:TAR_GZ, 2)
  field(:TAR_BZ2, 3)
end

defmodule Gitaly.GetRawChangesResponse.RawChange.Operation do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t ::
          integer | :UNKNOWN | :ADDED | :COPIED | :DELETED | :MODIFIED | :RENAMED | :TYPE_CHANGED

  field(:UNKNOWN, 0)
  field(:ADDED, 1)
  field(:COPIED, 2)
  field(:DELETED, 3)
  field(:MODIFIED, 4)
  field(:RENAMED, 5)
  field(:TYPE_CHANGED, 6)
end

defmodule Gitaly.RepositoryExistsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.RepositoryExistsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          exists: boolean
        }

  defstruct exists: false

  field(:exists, 1, type: :bool)
end

defmodule Gitaly.RepackIncrementalRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.RepackIncrementalResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.RepackFullRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          create_bitmap: boolean
        }

  defstruct repository: nil,
            create_bitmap: false

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:create_bitmap, 2, type: :bool, json_name: "createBitmap")
end

defmodule Gitaly.RepackFullResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.MidxRepackRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.MidxRepackResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.GarbageCollectRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          create_bitmap: boolean,
          prune: boolean
        }

  defstruct repository: nil,
            create_bitmap: false,
            prune: false

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:create_bitmap, 2, type: :bool, json_name: "createBitmap")
  field(:prune, 3, type: :bool)
end

defmodule Gitaly.GarbageCollectResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.WriteCommitGraphRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          splitStrategy: Gitaly.WriteCommitGraphRequest.SplitStrategy.t()
        }

  defstruct repository: nil,
            splitStrategy: :SizeMultiple

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:splitStrategy, 2, type: Gitaly.WriteCommitGraphRequest.SplitStrategy, enum: true)
end

defmodule Gitaly.WriteCommitGraphResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.CleanupRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.CleanupResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.RepositorySizeRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.RepositorySizeResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          size: integer
        }

  defstruct size: 0

  field(:size, 1, type: :int64)
end

defmodule Gitaly.ApplyGitattributesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revision: binary
        }

  defstruct repository: nil,
            revision: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revision, 2, type: :bytes)
end

defmodule Gitaly.ApplyGitattributesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.FetchBundleRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          data: binary,
          update_head: boolean
        }

  defstruct repository: nil,
            data: "",
            update_head: false

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:data, 2, type: :bytes)
  field(:update_head, 3, type: :bool, json_name: "updateHead")
end

defmodule Gitaly.FetchBundleResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.FetchRemoteRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          force: boolean,
          no_tags: boolean,
          timeout: integer,
          ssh_key: String.t(),
          known_hosts: String.t(),
          no_prune: boolean,
          remote_params: Gitaly.Remote.t() | nil,
          check_tags_changed: boolean
        }

  defstruct repository: nil,
            force: false,
            no_tags: false,
            timeout: 0,
            ssh_key: "",
            known_hosts: "",
            no_prune: false,
            remote_params: nil,
            check_tags_changed: false

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:force, 3, type: :bool)
  field(:no_tags, 4, type: :bool, json_name: "noTags")
  field(:timeout, 5, type: :int32)
  field(:ssh_key, 6, type: :string, json_name: "sshKey")
  field(:known_hosts, 7, type: :string, json_name: "knownHosts")
  field(:no_prune, 9, type: :bool, json_name: "noPrune")
  field(:remote_params, 10, type: Gitaly.Remote, json_name: "remoteParams")
  field(:check_tags_changed, 11, type: :bool, json_name: "checkTagsChanged")
end

defmodule Gitaly.FetchRemoteResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          tags_changed: boolean
        }

  defstruct tags_changed: false

  field(:tags_changed, 1, type: :bool, json_name: "tagsChanged")
end

defmodule Gitaly.CreateRepositoryRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.CreateRepositoryResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.GetArchiveRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          commit_id: String.t(),
          prefix: String.t(),
          format: Gitaly.GetArchiveRequest.Format.t(),
          path: binary,
          exclude: [binary],
          elide_path: boolean,
          include_lfs_blobs: boolean
        }

  defstruct repository: nil,
            commit_id: "",
            prefix: "",
            format: :ZIP,
            path: "",
            exclude: [],
            elide_path: false,
            include_lfs_blobs: false

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:commit_id, 2, type: :string, json_name: "commitId")
  field(:prefix, 3, type: :string)
  field(:format, 4, type: Gitaly.GetArchiveRequest.Format, enum: true)
  field(:path, 5, type: :bytes)
  field(:exclude, 6, repeated: true, type: :bytes)
  field(:elide_path, 7, type: :bool, json_name: "elidePath")
  field(:include_lfs_blobs, 8, type: :bool, json_name: "includeLfsBlobs")
end

defmodule Gitaly.GetArchiveResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          data: binary
        }

  defstruct data: ""

  field(:data, 1, type: :bytes)
end

defmodule Gitaly.HasLocalBranchesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.HasLocalBranchesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          value: boolean
        }

  defstruct value: false

  field(:value, 1, type: :bool)
end

defmodule Gitaly.FetchSourceBranchRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          source_repository: Gitaly.Repository.t() | nil,
          source_branch: binary,
          target_ref: binary
        }

  defstruct repository: nil,
            source_repository: nil,
            source_branch: "",
            target_ref: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:source_repository, 2, type: Gitaly.Repository, json_name: "sourceRepository")
  field(:source_branch, 3, type: :bytes, json_name: "sourceBranch")
  field(:target_ref, 4, type: :bytes, json_name: "targetRef")
end

defmodule Gitaly.FetchSourceBranchResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          result: boolean
        }

  defstruct result: false

  field(:result, 1, type: :bool)
end

defmodule Gitaly.FsckRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.FsckResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          error: binary
        }

  defstruct error: ""

  field(:error, 1, type: :bytes)
end

defmodule Gitaly.WriteRefRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          ref: binary,
          revision: binary,
          old_revision: binary,
          force: boolean
        }

  defstruct repository: nil,
            ref: "",
            revision: "",
            old_revision: "",
            force: false

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:ref, 2, type: :bytes)
  field(:revision, 3, type: :bytes)
  field(:old_revision, 4, type: :bytes, json_name: "oldRevision")
  field(:force, 5, type: :bool)
end

defmodule Gitaly.WriteRefResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.FindMergeBaseRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revisions: [binary]
        }

  defstruct repository: nil,
            revisions: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revisions, 2, repeated: true, type: :bytes)
end

defmodule Gitaly.FindMergeBaseResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          base: String.t()
        }

  defstruct base: ""

  field(:base, 1, type: :string)
end

defmodule Gitaly.CreateForkRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          source_repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil,
            source_repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:source_repository, 2, type: Gitaly.Repository, json_name: "sourceRepository")
end

defmodule Gitaly.CreateForkResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.CreateRepositoryFromURLRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          url: String.t()
        }

  defstruct repository: nil,
            url: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:url, 2, type: :string)
end

defmodule Gitaly.CreateRepositoryFromURLResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.CreateBundleRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.CreateBundleResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          data: binary
        }

  defstruct data: ""

  field(:data, 1, type: :bytes)
end

defmodule Gitaly.CreateBundleFromRefListRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          patterns: [binary]
        }

  defstruct repository: nil,
            patterns: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:patterns, 2, repeated: true, type: :bytes)
end

defmodule Gitaly.CreateBundleFromRefListResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          data: binary
        }

  defstruct data: ""

  field(:data, 1, type: :bytes)
end

defmodule Gitaly.GetConfigRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.GetConfigResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          data: binary
        }

  defstruct data: ""

  field(:data, 1, type: :bytes)
end

defmodule Gitaly.RestoreCustomHooksRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          data: binary
        }

  defstruct repository: nil,
            data: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:data, 2, type: :bytes)
end

defmodule Gitaly.RestoreCustomHooksResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.BackupCustomHooksRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.BackupCustomHooksResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          data: binary
        }

  defstruct data: ""

  field(:data, 1, type: :bytes)
end

defmodule Gitaly.CreateRepositoryFromBundleRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          data: binary
        }

  defstruct repository: nil,
            data: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:data, 2, type: :bytes)
end

defmodule Gitaly.CreateRepositoryFromBundleResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.FindLicenseRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.FindLicenseResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          license_short_name: String.t()
        }

  defstruct license_short_name: ""

  field(:license_short_name, 1, type: :string, json_name: "licenseShortName")
end

defmodule Gitaly.GetInfoAttributesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.GetInfoAttributesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          attributes: binary
        }

  defstruct attributes: ""

  field(:attributes, 1, type: :bytes)
end

defmodule Gitaly.CalculateChecksumRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.CalculateChecksumResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          checksum: String.t()
        }

  defstruct checksum: ""

  field(:checksum, 1, type: :string)
end

defmodule Gitaly.GetSnapshotRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.GetSnapshotResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          data: binary
        }

  defstruct data: ""

  field(:data, 1, type: :bytes)
end

defmodule Gitaly.CreateRepositoryFromSnapshotRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          http_url: String.t(),
          http_auth: String.t()
        }

  defstruct repository: nil,
            http_url: "",
            http_auth: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:http_url, 2, type: :string, json_name: "httpUrl")
  field(:http_auth, 3, type: :string, json_name: "httpAuth")
end

defmodule Gitaly.CreateRepositoryFromSnapshotResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.GetRawChangesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          from_revision: String.t(),
          to_revision: String.t()
        }

  defstruct repository: nil,
            from_revision: "",
            to_revision: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:from_revision, 2, type: :string, json_name: "fromRevision")
  field(:to_revision, 3, type: :string, json_name: "toRevision")
end

defmodule Gitaly.GetRawChangesResponse.RawChange do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          blob_id: String.t(),
          size: integer,
          new_path: String.t(),
          old_path: String.t(),
          operation: Gitaly.GetRawChangesResponse.RawChange.Operation.t(),
          raw_operation: String.t(),
          old_mode: integer,
          new_mode: integer,
          new_path_bytes: binary,
          old_path_bytes: binary
        }

  defstruct blob_id: "",
            size: 0,
            new_path: "",
            old_path: "",
            operation: :UNKNOWN,
            raw_operation: "",
            old_mode: 0,
            new_mode: 0,
            new_path_bytes: "",
            old_path_bytes: ""

  field(:blob_id, 1, type: :string, json_name: "blobId")
  field(:size, 2, type: :int64)
  field(:new_path, 3, type: :string, json_name: "newPath", deprecated: true)
  field(:old_path, 4, type: :string, json_name: "oldPath", deprecated: true)
  field(:operation, 5, type: Gitaly.GetRawChangesResponse.RawChange.Operation, enum: true)
  field(:raw_operation, 6, type: :string, json_name: "rawOperation")
  field(:old_mode, 7, type: :int32, json_name: "oldMode")
  field(:new_mode, 8, type: :int32, json_name: "newMode")
  field(:new_path_bytes, 9, type: :bytes, json_name: "newPathBytes")
  field(:old_path_bytes, 10, type: :bytes, json_name: "oldPathBytes")
end

defmodule Gitaly.GetRawChangesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          raw_changes: [Gitaly.GetRawChangesResponse.RawChange.t()]
        }

  defstruct raw_changes: []

  field(:raw_changes, 1,
    repeated: true,
    type: Gitaly.GetRawChangesResponse.RawChange,
    json_name: "rawChanges"
  )
end

defmodule Gitaly.SearchFilesByNameRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          query: String.t(),
          ref: binary,
          filter: String.t()
        }

  defstruct repository: nil,
            query: "",
            ref: "",
            filter: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:query, 2, type: :string)
  field(:ref, 3, type: :bytes)
  field(:filter, 4, type: :string)
end

defmodule Gitaly.SearchFilesByNameResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          files: [binary]
        }

  defstruct files: []

  field(:files, 1, repeated: true, type: :bytes)
end

defmodule Gitaly.SearchFilesByContentRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          query: String.t(),
          ref: binary,
          chunked_response: boolean
        }

  defstruct repository: nil,
            query: "",
            ref: "",
            chunked_response: false

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:query, 2, type: :string)
  field(:ref, 3, type: :bytes)
  field(:chunked_response, 4, type: :bool, json_name: "chunkedResponse")
end

defmodule Gitaly.SearchFilesByContentResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          matches: [binary],
          match_data: binary,
          end_of_match: boolean
        }

  defstruct matches: [],
            match_data: "",
            end_of_match: false

  field(:matches, 1, repeated: true, type: :bytes)
  field(:match_data, 2, type: :bytes, json_name: "matchData")
  field(:end_of_match, 3, type: :bool, json_name: "endOfMatch")
end

defmodule Gitaly.Remote do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          url: String.t(),
          http_authorization_header: String.t(),
          mirror_refmaps: [String.t()]
        }

  defstruct url: "",
            http_authorization_header: "",
            mirror_refmaps: []

  field(:url, 1, type: :string)
  field(:http_authorization_header, 3, type: :string, json_name: "httpAuthorizationHeader")
  field(:mirror_refmaps, 4, repeated: true, type: :string, json_name: "mirrorRefmaps")
end

defmodule Gitaly.GetObjectDirectorySizeRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.GetObjectDirectorySizeResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          size: integer
        }

  defstruct size: 0

  field(:size, 1, type: :int64)
end

defmodule Gitaly.RemoveRepositoryRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.RemoveRepositoryResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.RenameRepositoryRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          relative_path: String.t()
        }

  defstruct repository: nil,
            relative_path: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:relative_path, 2, type: :string, json_name: "relativePath")
end

defmodule Gitaly.RenameRepositoryResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.ReplicateRepositoryRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          source: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil,
            source: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:source, 2, type: Gitaly.Repository)
end

defmodule Gitaly.ReplicateRepositoryResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.OptimizeRepositoryRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.OptimizeRepositoryResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.SetFullPathRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          path: String.t()
        }

  defstruct repository: nil,
            path: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:path, 2, type: :string)
end

defmodule Gitaly.SetFullPathResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.RepositoryService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.RepositoryService"

  rpc(:RepositoryExists, Gitaly.RepositoryExistsRequest, Gitaly.RepositoryExistsResponse)

  rpc(:RepackIncremental, Gitaly.RepackIncrementalRequest, Gitaly.RepackIncrementalResponse)

  rpc(:RepackFull, Gitaly.RepackFullRequest, Gitaly.RepackFullResponse)

  rpc(:MidxRepack, Gitaly.MidxRepackRequest, Gitaly.MidxRepackResponse)

  rpc(:GarbageCollect, Gitaly.GarbageCollectRequest, Gitaly.GarbageCollectResponse)

  rpc(:WriteCommitGraph, Gitaly.WriteCommitGraphRequest, Gitaly.WriteCommitGraphResponse)

  rpc(:RepositorySize, Gitaly.RepositorySizeRequest, Gitaly.RepositorySizeResponse)

  rpc(:ApplyGitattributes, Gitaly.ApplyGitattributesRequest, Gitaly.ApplyGitattributesResponse)

  rpc(:FetchRemote, Gitaly.FetchRemoteRequest, Gitaly.FetchRemoteResponse)

  rpc(:CreateRepository, Gitaly.CreateRepositoryRequest, Gitaly.CreateRepositoryResponse)

  rpc(:GetArchive, Gitaly.GetArchiveRequest, stream(Gitaly.GetArchiveResponse))

  rpc(:HasLocalBranches, Gitaly.HasLocalBranchesRequest, Gitaly.HasLocalBranchesResponse)

  rpc(:FetchSourceBranch, Gitaly.FetchSourceBranchRequest, Gitaly.FetchSourceBranchResponse)

  rpc(:Fsck, Gitaly.FsckRequest, Gitaly.FsckResponse)

  rpc(:WriteRef, Gitaly.WriteRefRequest, Gitaly.WriteRefResponse)

  rpc(:FindMergeBase, Gitaly.FindMergeBaseRequest, Gitaly.FindMergeBaseResponse)

  rpc(:CreateFork, Gitaly.CreateForkRequest, Gitaly.CreateForkResponse)

  rpc(
    :CreateRepositoryFromURL,
    Gitaly.CreateRepositoryFromURLRequest,
    Gitaly.CreateRepositoryFromURLResponse
  )

  rpc(:CreateBundle, Gitaly.CreateBundleRequest, stream(Gitaly.CreateBundleResponse))

  rpc(
    :CreateBundleFromRefList,
    stream(Gitaly.CreateBundleFromRefListRequest),
    stream(Gitaly.CreateBundleFromRefListResponse)
  )

  rpc(:FetchBundle, stream(Gitaly.FetchBundleRequest), Gitaly.FetchBundleResponse)

  rpc(
    :CreateRepositoryFromBundle,
    stream(Gitaly.CreateRepositoryFromBundleRequest),
    Gitaly.CreateRepositoryFromBundleResponse
  )

  rpc(:GetConfig, Gitaly.GetConfigRequest, stream(Gitaly.GetConfigResponse))

  rpc(:FindLicense, Gitaly.FindLicenseRequest, Gitaly.FindLicenseResponse)

  rpc(
    :GetInfoAttributes,
    Gitaly.GetInfoAttributesRequest,
    stream(Gitaly.GetInfoAttributesResponse)
  )

  rpc(:CalculateChecksum, Gitaly.CalculateChecksumRequest, Gitaly.CalculateChecksumResponse)

  rpc(:Cleanup, Gitaly.CleanupRequest, Gitaly.CleanupResponse)

  rpc(:GetSnapshot, Gitaly.GetSnapshotRequest, stream(Gitaly.GetSnapshotResponse))

  rpc(
    :CreateRepositoryFromSnapshot,
    Gitaly.CreateRepositoryFromSnapshotRequest,
    Gitaly.CreateRepositoryFromSnapshotResponse
  )

  rpc(:GetRawChanges, Gitaly.GetRawChangesRequest, stream(Gitaly.GetRawChangesResponse))

  rpc(
    :SearchFilesByContent,
    Gitaly.SearchFilesByContentRequest,
    stream(Gitaly.SearchFilesByContentResponse)
  )

  rpc(
    :SearchFilesByName,
    Gitaly.SearchFilesByNameRequest,
    stream(Gitaly.SearchFilesByNameResponse)
  )

  rpc(
    :RestoreCustomHooks,
    stream(Gitaly.RestoreCustomHooksRequest),
    Gitaly.RestoreCustomHooksResponse
  )

  rpc(
    :BackupCustomHooks,
    Gitaly.BackupCustomHooksRequest,
    stream(Gitaly.BackupCustomHooksResponse)
  )

  rpc(
    :GetObjectDirectorySize,
    Gitaly.GetObjectDirectorySizeRequest,
    Gitaly.GetObjectDirectorySizeResponse
  )

  rpc(:RemoveRepository, Gitaly.RemoveRepositoryRequest, Gitaly.RemoveRepositoryResponse)

  rpc(:RenameRepository, Gitaly.RenameRepositoryRequest, Gitaly.RenameRepositoryResponse)

  rpc(:ReplicateRepository, Gitaly.ReplicateRepositoryRequest, Gitaly.ReplicateRepositoryResponse)

  rpc(:OptimizeRepository, Gitaly.OptimizeRepositoryRequest, Gitaly.OptimizeRepositoryResponse)

  rpc(:SetFullPath, Gitaly.SetFullPathRequest, Gitaly.SetFullPathResponse)
end

defmodule Gitaly.RepositoryService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.RepositoryService.Service
end
