defmodule Gitaly.ApplyBfgObjectMapStreamRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          object_map: binary
        }

  defstruct repository: nil,
            object_map: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:object_map, 2, type: :bytes, json_name: "objectMap")
end

defmodule Gitaly.ApplyBfgObjectMapStreamResponse.Entry do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          type: Gitaly.ObjectType.t(),
          old_oid: String.t(),
          new_oid: String.t()
        }

  defstruct type: :UNKNOWN,
            old_oid: "",
            new_oid: ""

  field(:type, 1, type: Gitaly.ObjectType, enum: true)
  field(:old_oid, 2, type: :string, json_name: "oldOid")
  field(:new_oid, 3, type: :string, json_name: "newOid")
end

defmodule Gitaly.ApplyBfgObjectMapStreamResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          entries: [Gitaly.ApplyBfgObjectMapStreamResponse.Entry.t()]
        }

  defstruct entries: []

  field(:entries, 1, repeated: true, type: Gitaly.ApplyBfgObjectMapStreamResponse.Entry)
end

defmodule Gitaly.CleanupService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.CleanupService"

  rpc(
    :ApplyBfgObjectMapStream,
    stream(Gitaly.ApplyBfgObjectMapStreamRequest),
    stream(Gitaly.ApplyBfgObjectMapStreamResponse)
  )
end

defmodule Gitaly.CleanupService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.CleanupService.Service
end
