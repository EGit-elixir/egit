defmodule Gitaly.UpdateRemoteMirrorRequest.Remote do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          url: String.t(),
          http_authorization_header: String.t()
        }

  defstruct url: "",
            http_authorization_header: ""

  field(:url, 1, type: :string)
  field(:http_authorization_header, 2, type: :string, json_name: "httpAuthorizationHeader")
end

defmodule Gitaly.UpdateRemoteMirrorRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          remote: Gitaly.UpdateRemoteMirrorRequest.Remote.t() | nil,
          only_branches_matching: [binary],
          ssh_key: String.t(),
          known_hosts: String.t(),
          keep_divergent_refs: boolean
        }

  defstruct repository: nil,
            remote: nil,
            only_branches_matching: [],
            ssh_key: "",
            known_hosts: "",
            keep_divergent_refs: false

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:remote, 7, type: Gitaly.UpdateRemoteMirrorRequest.Remote)

  field(:only_branches_matching, 3,
    repeated: true,
    type: :bytes,
    json_name: "onlyBranchesMatching"
  )

  field(:ssh_key, 4, type: :string, json_name: "sshKey")
  field(:known_hosts, 5, type: :string, json_name: "knownHosts")
  field(:keep_divergent_refs, 6, type: :bool, json_name: "keepDivergentRefs")
end

defmodule Gitaly.UpdateRemoteMirrorResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          divergent_refs: [binary]
        }

  defstruct divergent_refs: []

  field(:divergent_refs, 1, repeated: true, type: :bytes, json_name: "divergentRefs")
end

defmodule Gitaly.FindRemoteRepositoryRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          remote: String.t(),
          storage_name: String.t()
        }

  defstruct remote: "",
            storage_name: ""

  field(:remote, 1, type: :string)
  field(:storage_name, 2, type: :string, json_name: "storageName", deprecated: false)
end

defmodule Gitaly.FindRemoteRepositoryResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          exists: boolean
        }

  defstruct exists: false

  field(:exists, 1, type: :bool)
end

defmodule Gitaly.FindRemoteRootRefRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          remote_url: String.t(),
          http_authorization_header: String.t()
        }

  defstruct repository: nil,
            remote_url: "",
            http_authorization_header: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:remote_url, 3, type: :string, json_name: "remoteUrl")
  field(:http_authorization_header, 4, type: :string, json_name: "httpAuthorizationHeader")
end

defmodule Gitaly.FindRemoteRootRefResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          ref: String.t()
        }

  defstruct ref: ""

  field(:ref, 1, type: :string)
end

defmodule Gitaly.RemoteService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.RemoteService"

  rpc(
    :UpdateRemoteMirror,
    stream(Gitaly.UpdateRemoteMirrorRequest),
    Gitaly.UpdateRemoteMirrorResponse
  )

  rpc(
    :FindRemoteRepository,
    Gitaly.FindRemoteRepositoryRequest,
    Gitaly.FindRemoteRepositoryResponse
  )

  rpc(:FindRemoteRootRef, Gitaly.FindRemoteRootRefRequest, Gitaly.FindRemoteRootRefResponse)
end

defmodule Gitaly.RemoteService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.RemoteService.Service
end
