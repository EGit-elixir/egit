defmodule Gitaly.ServerInfoRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.ServerInfoResponse.StorageStatus do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          storage_name: String.t(),
          readable: boolean,
          writeable: boolean,
          fs_type: String.t(),
          filesystem_id: String.t(),
          replication_factor: non_neg_integer
        }

  defstruct storage_name: "",
            readable: false,
            writeable: false,
            fs_type: "",
            filesystem_id: "",
            replication_factor: 0

  field(:storage_name, 1, type: :string, json_name: "storageName")
  field(:readable, 2, type: :bool)
  field(:writeable, 3, type: :bool)
  field(:fs_type, 4, type: :string, json_name: "fsType")
  field(:filesystem_id, 5, type: :string, json_name: "filesystemId")
  field(:replication_factor, 6, type: :uint32, json_name: "replicationFactor")
end

defmodule Gitaly.ServerInfoResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          server_version: String.t(),
          git_version: String.t(),
          storage_statuses: [Gitaly.ServerInfoResponse.StorageStatus.t()]
        }

  defstruct server_version: "",
            git_version: "",
            storage_statuses: []

  field(:server_version, 1, type: :string, json_name: "serverVersion")
  field(:git_version, 2, type: :string, json_name: "gitVersion")

  field(:storage_statuses, 3,
    repeated: true,
    type: Gitaly.ServerInfoResponse.StorageStatus,
    json_name: "storageStatuses"
  )
end

defmodule Gitaly.DiskStatisticsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.DiskStatisticsResponse.StorageStatus do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          storage_name: String.t(),
          available: integer,
          used: integer
        }

  defstruct storage_name: "",
            available: 0,
            used: 0

  field(:storage_name, 1, type: :string, json_name: "storageName")
  field(:available, 2, type: :int64)
  field(:used, 3, type: :int64)
end

defmodule Gitaly.DiskStatisticsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          storage_statuses: [Gitaly.DiskStatisticsResponse.StorageStatus.t()]
        }

  defstruct storage_statuses: []

  field(:storage_statuses, 1,
    repeated: true,
    type: Gitaly.DiskStatisticsResponse.StorageStatus,
    json_name: "storageStatuses"
  )
end

defmodule Gitaly.ServerService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.ServerService"

  rpc(:ServerInfo, Gitaly.ServerInfoRequest, Gitaly.ServerInfoResponse)

  rpc(:DiskStatistics, Gitaly.DiskStatisticsRequest, Gitaly.DiskStatisticsResponse)
end

defmodule Gitaly.ServerService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.ServerService.Service
end
