defmodule Gitaly.InfoRefsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          git_config_options: [String.t()],
          git_protocol: String.t()
        }

  defstruct repository: nil,
            git_config_options: [],
            git_protocol: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:git_config_options, 2, repeated: true, type: :string, json_name: "gitConfigOptions")
  field(:git_protocol, 3, type: :string, json_name: "gitProtocol")
end

defmodule Gitaly.InfoRefsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          data: binary
        }

  defstruct data: ""

  field(:data, 1, type: :bytes)
end

defmodule Gitaly.PostUploadPackRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          data: binary,
          git_config_options: [String.t()],
          git_protocol: String.t()
        }

  defstruct repository: nil,
            data: "",
            git_config_options: [],
            git_protocol: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:data, 2, type: :bytes)
  field(:git_config_options, 3, repeated: true, type: :string, json_name: "gitConfigOptions")
  field(:git_protocol, 4, type: :string, json_name: "gitProtocol")
end

defmodule Gitaly.PostUploadPackResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          data: binary
        }

  defstruct data: ""

  field(:data, 1, type: :bytes)
end

defmodule Gitaly.PostUploadPackWithSidechannelRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          git_config_options: [String.t()],
          git_protocol: String.t()
        }

  defstruct repository: nil,
            git_config_options: [],
            git_protocol: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:git_config_options, 2, repeated: true, type: :string, json_name: "gitConfigOptions")
  field(:git_protocol, 3, type: :string, json_name: "gitProtocol")
end

defmodule Gitaly.PostUploadPackWithSidechannelResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.PostReceivePackRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          data: binary,
          gl_id: String.t(),
          gl_repository: String.t(),
          gl_username: String.t(),
          git_protocol: String.t(),
          git_config_options: [String.t()]
        }

  defstruct repository: nil,
            data: "",
            gl_id: "",
            gl_repository: "",
            gl_username: "",
            git_protocol: "",
            git_config_options: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:data, 2, type: :bytes)
  field(:gl_id, 3, type: :string, json_name: "glId")
  field(:gl_repository, 4, type: :string, json_name: "glRepository")
  field(:gl_username, 5, type: :string, json_name: "glUsername")
  field(:git_protocol, 6, type: :string, json_name: "gitProtocol")
  field(:git_config_options, 7, repeated: true, type: :string, json_name: "gitConfigOptions")
end

defmodule Gitaly.PostReceivePackResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          data: binary
        }

  defstruct data: ""

  field(:data, 1, type: :bytes)
end

defmodule Gitaly.SmartHTTPService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.SmartHTTPService"

  rpc(:InfoRefsUploadPack, Gitaly.InfoRefsRequest, stream(Gitaly.InfoRefsResponse))

  rpc(:InfoRefsReceivePack, Gitaly.InfoRefsRequest, stream(Gitaly.InfoRefsResponse))

  rpc(
    :PostUploadPack,
    stream(Gitaly.PostUploadPackRequest),
    stream(Gitaly.PostUploadPackResponse)
  )

  rpc(
    :PostUploadPackWithSidechannel,
    Gitaly.PostUploadPackWithSidechannelRequest,
    Gitaly.PostUploadPackWithSidechannelResponse
  )

  rpc(
    :PostReceivePack,
    stream(Gitaly.PostReceivePackRequest),
    stream(Gitaly.PostReceivePackResponse)
  )
end

defmodule Gitaly.SmartHTTPService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.SmartHTTPService.Service
end
