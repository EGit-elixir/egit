defmodule Gitaly.CommitDiffRequest.DiffMode do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :DEFAULT | :WORDDIFF

  field(:DEFAULT, 0)
  field(:WORDDIFF, 1)
end

defmodule Gitaly.ChangedPaths.Status do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :ADDED | :MODIFIED | :DELETED | :TYPE_CHANGE | :COPIED

  field(:ADDED, 0)
  field(:MODIFIED, 1)
  field(:DELETED, 2)
  field(:TYPE_CHANGE, 3)
  field(:COPIED, 4)
end

defmodule Gitaly.CommitDiffRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          left_commit_id: String.t(),
          right_commit_id: String.t(),
          ignore_whitespace_change: boolean,
          paths: [binary],
          collapse_diffs: boolean,
          enforce_limits: boolean,
          max_files: integer,
          max_lines: integer,
          max_bytes: integer,
          max_patch_bytes: integer,
          safe_max_files: integer,
          safe_max_lines: integer,
          safe_max_bytes: integer,
          diff_mode: Gitaly.CommitDiffRequest.DiffMode.t()
        }

  defstruct repository: nil,
            left_commit_id: "",
            right_commit_id: "",
            ignore_whitespace_change: false,
            paths: [],
            collapse_diffs: false,
            enforce_limits: false,
            max_files: 0,
            max_lines: 0,
            max_bytes: 0,
            max_patch_bytes: 0,
            safe_max_files: 0,
            safe_max_lines: 0,
            safe_max_bytes: 0,
            diff_mode: :DEFAULT

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:left_commit_id, 2, type: :string, json_name: "leftCommitId")
  field(:right_commit_id, 3, type: :string, json_name: "rightCommitId")
  field(:ignore_whitespace_change, 4, type: :bool, json_name: "ignoreWhitespaceChange")
  field(:paths, 5, repeated: true, type: :bytes)
  field(:collapse_diffs, 6, type: :bool, json_name: "collapseDiffs")
  field(:enforce_limits, 7, type: :bool, json_name: "enforceLimits")
  field(:max_files, 8, type: :int32, json_name: "maxFiles")
  field(:max_lines, 9, type: :int32, json_name: "maxLines")
  field(:max_bytes, 10, type: :int32, json_name: "maxBytes")
  field(:max_patch_bytes, 14, type: :int32, json_name: "maxPatchBytes")
  field(:safe_max_files, 11, type: :int32, json_name: "safeMaxFiles")
  field(:safe_max_lines, 12, type: :int32, json_name: "safeMaxLines")
  field(:safe_max_bytes, 13, type: :int32, json_name: "safeMaxBytes")

  field(:diff_mode, 15, type: Gitaly.CommitDiffRequest.DiffMode, json_name: "diffMode", enum: true)
end

defmodule Gitaly.CommitDiffResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          from_path: binary,
          to_path: binary,
          from_id: String.t(),
          to_id: String.t(),
          old_mode: integer,
          new_mode: integer,
          binary: boolean,
          raw_patch_data: binary,
          end_of_patch: boolean,
          overflow_marker: boolean,
          collapsed: boolean,
          too_large: boolean
        }

  defstruct from_path: "",
            to_path: "",
            from_id: "",
            to_id: "",
            old_mode: 0,
            new_mode: 0,
            binary: false,
            raw_patch_data: "",
            end_of_patch: false,
            overflow_marker: false,
            collapsed: false,
            too_large: false

  field(:from_path, 1, type: :bytes, json_name: "fromPath")
  field(:to_path, 2, type: :bytes, json_name: "toPath")
  field(:from_id, 3, type: :string, json_name: "fromId")
  field(:to_id, 4, type: :string, json_name: "toId")
  field(:old_mode, 5, type: :int32, json_name: "oldMode")
  field(:new_mode, 6, type: :int32, json_name: "newMode")
  field(:binary, 7, type: :bool)
  field(:raw_patch_data, 9, type: :bytes, json_name: "rawPatchData")
  field(:end_of_patch, 10, type: :bool, json_name: "endOfPatch")
  field(:overflow_marker, 11, type: :bool, json_name: "overflowMarker")
  field(:collapsed, 12, type: :bool)
  field(:too_large, 13, type: :bool, json_name: "tooLarge")
end

defmodule Gitaly.CommitDeltaRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          left_commit_id: String.t(),
          right_commit_id: String.t(),
          paths: [binary]
        }

  defstruct repository: nil,
            left_commit_id: "",
            right_commit_id: "",
            paths: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:left_commit_id, 2, type: :string, json_name: "leftCommitId")
  field(:right_commit_id, 3, type: :string, json_name: "rightCommitId")
  field(:paths, 4, repeated: true, type: :bytes)
end

defmodule Gitaly.CommitDelta do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          from_path: binary,
          to_path: binary,
          from_id: String.t(),
          to_id: String.t(),
          old_mode: integer,
          new_mode: integer
        }

  defstruct from_path: "",
            to_path: "",
            from_id: "",
            to_id: "",
            old_mode: 0,
            new_mode: 0

  field(:from_path, 1, type: :bytes, json_name: "fromPath")
  field(:to_path, 2, type: :bytes, json_name: "toPath")
  field(:from_id, 3, type: :string, json_name: "fromId")
  field(:to_id, 4, type: :string, json_name: "toId")
  field(:old_mode, 5, type: :int32, json_name: "oldMode")
  field(:new_mode, 6, type: :int32, json_name: "newMode")
end

defmodule Gitaly.CommitDeltaResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          deltas: [Gitaly.CommitDelta.t()]
        }

  defstruct deltas: []

  field(:deltas, 1, repeated: true, type: Gitaly.CommitDelta)
end

defmodule Gitaly.RawDiffRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          left_commit_id: String.t(),
          right_commit_id: String.t()
        }

  defstruct repository: nil,
            left_commit_id: "",
            right_commit_id: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:left_commit_id, 2, type: :string, json_name: "leftCommitId")
  field(:right_commit_id, 3, type: :string, json_name: "rightCommitId")
end

defmodule Gitaly.RawDiffResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          data: binary
        }

  defstruct data: ""

  field(:data, 1, type: :bytes)
end

defmodule Gitaly.RawPatchRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          left_commit_id: String.t(),
          right_commit_id: String.t()
        }

  defstruct repository: nil,
            left_commit_id: "",
            right_commit_id: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:left_commit_id, 2, type: :string, json_name: "leftCommitId")
  field(:right_commit_id, 3, type: :string, json_name: "rightCommitId")
end

defmodule Gitaly.RawPatchResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          data: binary
        }

  defstruct data: ""

  field(:data, 1, type: :bytes)
end

defmodule Gitaly.DiffStatsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          left_commit_id: String.t(),
          right_commit_id: String.t()
        }

  defstruct repository: nil,
            left_commit_id: "",
            right_commit_id: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:left_commit_id, 2, type: :string, json_name: "leftCommitId")
  field(:right_commit_id, 3, type: :string, json_name: "rightCommitId")
end

defmodule Gitaly.DiffStats do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          path: binary,
          additions: integer,
          deletions: integer,
          old_path: binary
        }

  defstruct path: "",
            additions: 0,
            deletions: 0,
            old_path: ""

  field(:path, 1, type: :bytes)
  field(:additions, 2, type: :int32)
  field(:deletions, 3, type: :int32)
  field(:old_path, 4, type: :bytes, json_name: "oldPath")
end

defmodule Gitaly.DiffStatsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          stats: [Gitaly.DiffStats.t()]
        }

  defstruct stats: []

  field(:stats, 1, repeated: true, type: Gitaly.DiffStats)
end

defmodule Gitaly.FindChangedPathsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          commits: [String.t()]
        }

  defstruct repository: nil,
            commits: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:commits, 2, repeated: true, type: :string)
end

defmodule Gitaly.FindChangedPathsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          paths: [Gitaly.ChangedPaths.t()]
        }

  defstruct paths: []

  field(:paths, 1, repeated: true, type: Gitaly.ChangedPaths)
end

defmodule Gitaly.ChangedPaths do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          path: binary,
          status: Gitaly.ChangedPaths.Status.t()
        }

  defstruct path: "",
            status: :ADDED

  field(:path, 1, type: :bytes)
  field(:status, 2, type: Gitaly.ChangedPaths.Status, enum: true)
end

defmodule Gitaly.DiffService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.DiffService"

  rpc(:CommitDiff, Gitaly.CommitDiffRequest, stream(Gitaly.CommitDiffResponse))

  rpc(:CommitDelta, Gitaly.CommitDeltaRequest, stream(Gitaly.CommitDeltaResponse))

  rpc(:RawDiff, Gitaly.RawDiffRequest, stream(Gitaly.RawDiffResponse))

  rpc(:RawPatch, Gitaly.RawPatchRequest, stream(Gitaly.RawPatchResponse))

  rpc(:DiffStats, Gitaly.DiffStatsRequest, stream(Gitaly.DiffStatsResponse))

  rpc(:FindChangedPaths, Gitaly.FindChangedPathsRequest, stream(Gitaly.FindChangedPathsResponse))
end

defmodule Gitaly.DiffService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.DiffService.Service
end
