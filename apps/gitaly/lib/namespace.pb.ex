defmodule Gitaly.AddNamespaceRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          storage_name: String.t(),
          name: String.t()
        }

  defstruct storage_name: "",
            name: ""

  field(:storage_name, 1, type: :string, json_name: "storageName", deprecated: false)
  field(:name, 2, type: :string)
end

defmodule Gitaly.RemoveNamespaceRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          storage_name: String.t(),
          name: String.t()
        }

  defstruct storage_name: "",
            name: ""

  field(:storage_name, 1, type: :string, json_name: "storageName", deprecated: false)
  field(:name, 2, type: :string)
end

defmodule Gitaly.RenameNamespaceRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          storage_name: String.t(),
          from: String.t(),
          to: String.t()
        }

  defstruct storage_name: "",
            from: "",
            to: ""

  field(:storage_name, 1, type: :string, json_name: "storageName", deprecated: false)
  field(:from, 2, type: :string)
  field(:to, 3, type: :string)
end

defmodule Gitaly.NamespaceExistsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          storage_name: String.t(),
          name: String.t()
        }

  defstruct storage_name: "",
            name: ""

  field(:storage_name, 1, type: :string, json_name: "storageName", deprecated: false)
  field(:name, 2, type: :string)
end

defmodule Gitaly.NamespaceExistsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          exists: boolean
        }

  defstruct exists: false

  field(:exists, 1, type: :bool)
end

defmodule Gitaly.AddNamespaceResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.RemoveNamespaceResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.RenameNamespaceResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.NamespaceService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.NamespaceService"

  rpc(:AddNamespace, Gitaly.AddNamespaceRequest, Gitaly.AddNamespaceResponse)

  rpc(:RemoveNamespace, Gitaly.RemoveNamespaceRequest, Gitaly.RemoveNamespaceResponse)

  rpc(:RenameNamespace, Gitaly.RenameNamespaceRequest, Gitaly.RenameNamespaceResponse)

  rpc(:NamespaceExists, Gitaly.NamespaceExistsRequest, Gitaly.NamespaceExistsResponse)
end

defmodule Gitaly.NamespaceService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.NamespaceService.Service
end
