{
  description = "Gitrekt";

  outputs = { self, nixpkgs }: let
      systems =
        [ "x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin" ];

      forAllSystems = f: nixpkgs.lib.genAttrs systems (system: f system);

      nixpkgsFor = forAllSystems (system:
        import nixpkgs {
          inherit system;
          #overlays = [ self.overlay ];
        });

      gitalyConfig = pkgs: pkgs.writeText "gitaly.toml" ''
        socket_path = "/tmp/egit/gitaly.socket"
        bin_dir = "${pkgs.gitaly}/bin"
        prometheus_listen_addr = "localhost:9236"

        [git]
        bin_path = "${pkgs.git}/bin/git"

        [gitaly-ruby]
        dir = "${pkgs.gitaly.ruby}"

        [gitlab-shell]
        dir = "/tmp/egit/shell"

        [hoos]
        custom_hook_dir = "/tmp/egit/git-hooks"

        [[storage]]
        name = "egit"
        path = "/tmp/egit/repositories"
      '';
    in {

      devShell = forAllSystems (system:
        nixpkgsFor.${system}.mkShell {
          GITALY_TESTING_NO_GIT_HOOKS = 1; # TODO: remove
          GITALY_CONFIG = gitalyConfig nixpkgsFor.${system};
          packages = with nixpkgsFor.${system}; [
            elixir
            erlang

            libgit2
            gitaly
            gitaly.rubyEnv
            gitaly.rubyEnv.wrappedRuby
          ];

          shellHook = ''
            if [ ! -f /tmp/egit ]; then
              mkdir -p /tmp/egit/repositories/+gitaly/tmp
              mkdir -p /tmp/egit/git-hooks
              mkdir -p /tmp/egit/shell
              touch /tmp/egit/git-hooks/{post-receive,pre-receive,update}
              chmod +x /tmp/egit/git-hooks/{post-receive,pre-receive,update}
            fi
          '';
        });
  };
}
